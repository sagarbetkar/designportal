angular.module('DesignPortal', ['ui.router', 'ngResource', 'datatables', 'ngAnimate', 'satellizer', 'toastr', 'ui.bootstrap', 'ngTouch', 'ngFileUpload', 'ui.utils'])
  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$authProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $authProvider) {
    /**
     * Helper auth functions
     */

    var skipIfLoggedIn = ['$q', '$location', '$auth', function($q, $location, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.reject();
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }];

    var loginRequired = ['$q', '$location', '$auth', function($q, $location, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/login');
      }
      return deferred.promise;
    }];

    $stateProvider
      .state('login', {
        url: "/login",
        views: {
          '': {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
          }
        },
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
        //$localStorage.$reset();
      })

      .state('app', {
        url: "",
        abstract: true,
        views: {
          '': {
            templateUrl: 'views/layouts/app.html'
          },
          'header@app': {
            templateUrl: 'views/partials/header.html'
          },
          'footer@app': {
            templateUrl: 'views/partials/footer.html'
          }

        },
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('app.insert', {
        url: "/insert",
        views: {
          'main@app': {
            templateUrl: 'views/insert.html'
          }
        }
      })
      .state('app.logout', {
          url: "/logout",
          views: {
              'main@app': {
                  controller: 'LogoutCtrl'
              }
          }
      });
    //$urlRouterProvider.otherwise('/login');
    $authProvider.tokenRoot = null;
    $authProvider.loginUrl = 'http://localhost/designportal/v1/auth/login';
    $authProvider.signupUrl = 'http://localhost/designportal/v1/signup';
    $authProvider.tokenName = 'token';
    $authProvider.tokenHeader = 'Authorization';
    //$authProvider.tokenType = 'Bearer';
    $authProvider.tokenType = '';
    $authProvider.storageType = 'localStorage';
  }])
