angular.module('DesignPortal')
    .controller('LoginCtrl',['$scope', '$state', 'Login', 'toastr', '$auth', function($scope, $state, Login, toastr, $auth){
        $scope.login = {};
        $scope.submit = function() {

        $auth.login($scope.user)
        .then(function(response) {
          if (response.data.message == "Login failed. Incorrect credentials"){
            toastr.error('Login failed', 'Incorrect credentials');
          }
          else{

                toastr.success('Login success','You have successfully signed in!');
                $state.go("app.insert");
          }
        })
         //$localStorage.$reset();
        .catch(function(error) {
          toastr.error(error.data.message, error.status);
        });

        }
        }])
    .controller('LogoutCtrl',['$scope', '$state', 'Login', 'toastr', '$auth', function($scope, $state, Login, toastr, $auth){
if (!$auth.isAuthenticated()) { return; }
    $auth.logout()
      .then(function() {
        toastr.info('You have been logged out');
        $state.go('login');
      });
        }])
