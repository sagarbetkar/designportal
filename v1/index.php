<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';
require '../vendor/autoload.php';

include '../include/Config.php';
$config = new Config();
$db = new DbHandler();

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
require_once '../include/auth_filters.php';
// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();
        global $user;
        // get the api key
        $api_key = $headers['Authorization'];
        $user = is_logged_in($api_key, '', $db);
        // validating api key
        // if (!$db->isValidApiKey($api_key)) {
        //     // api key is not present in users table
        //     $response["error"] = true;
        //     $response["message"] = "Access Denied. Invalid Api key";
        //     echoRespnse(401, $response);
        //     $app->stop();
        // } else {
        //     global $user_id;
        //     // get user primary key id
        //     $user_id = $db->getUserId($api_key);
        // }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->group('/auth', function () use ($app, $db, $config) {

$app->post('/register', function() use ($app, $config, $db) {

            $response = array();
            $json = $app->request->getBody();
            $data = json_decode($json, true);

            // reading post params
            $name = $data['name'];
            $email = $data['email'];
            $password = $data['password'];

            $db = new DbHandler();
            $res = $db->createUser($name, $email, $password);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app, $db, $config) {
            $response = array();
            $json = $app->request->getBody();
            $data = json_decode($json, true);

            // reading post params
            $email = $data['email'];
            $password = $data['password'];

            // get the user by email
            $user = $db->getUserByEmail($email);
            $db = new DbHandler();

            // check for correct email and password
            if ($db->checkLogin($email, $password)) {

                if ($user != NULL) {
                    $user_id = $user['id'];
                    $company_id = $user['id'];
                    $token = create_token($user_id, $company_id, $app->request->getUrl(), $config->getSecret('TOKEN_SECRET'));
                    $response["error"] = false;
                    $response['name'] = $user['name'];
                    $response['email'] = $user['email'];
                    $response['token'] = $token;
                    $response['createdAt'] = $user['created_at'];
                    $response["message"] = "You are successfully logged in";
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });
});
/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

 $app->post('/designs', 'authenticate', function() use ($app) {
   $response = array();
   $json = $app->request->getbody();
   $data = json_decode($json, true);

   //reading post params
   $name = $data['name'];
   $shortdescription = $data['shortdescription'];
   $description = $data['description'];
   $path = $data['path'];
   $status = $data['status'];

   $db = new DbHandler();
   $res = $db->createDesigns($name, $shortdescription, $description, $path, $status);

   if($res == DESIGN_UPLOADED_SUCCESSFULLY) {
     $response["error"] = false;
     $response["message"] = "You succesfully uploaded a design.";
   } else if ($res == DESIGN_UPLOADED_FAILED) {
     $response["error"] = true;
     $response["message"] = "Oops! An error occurred while uploading a design."
   }
 });

 $app->get('/designs', 'authenticate', function() use ($app) {
   $response = array();
   $db = new DbHandler();
   //fetching all designs
   $result = $db->getDesigns();

   $response["error"] = false;
   $response["data"] = array();

   while ($data = $result->fetch_assoc()) {
     $tmp = array();
     $tmp["id"] = $data["id"];
     $tmp["name"] = $data["name"];
     $tmp["shortdescription"] = $data["shortdescription"];
     $tmp["description"] = $data["description"];
     $tmp["path"] = $data["path"];
     $tmp["status"] = $data["status"];
     array_push($response["data"], $tmp);
   }
   echoRespnse(200, $response);
 });

 $app->get('/designs/:id', 'authenticate', function($design_id) use ($app) {
   $response = array();
   $db = new DbHandler();

   //fetching designs by id
   $result = $db->getDesignsById($design_id);

   $response["error"] = false;
   $response["data"] = array();

   while ($data = $result->fetch_assoc()) {
     $tmp = array();
     $tmp["id"] = $data["id"];
     $tmp["name"] = $data["name"];
     $tmp["shortdescription"] = $data["shortdescription"];
     $tmp["description"] = $data["description"];
     $tmp["path"] = $data["path"];
     $tmp["status"] = $data["status"];
     array_push($response["data"], $tmp);
   }
   echoRespnse(200, $response);
 });

 $app->put('/designs/:id', 'authenticate', function($design_id) use ($app) {
   $db = new DbHandler();
   $response = array();
   $json = $app->request->getbody();
   $data = json_decode($json, true);

   //reading put params
   $name = $data['name'];
   $shortdescription = $data['shortdescription'];
   $description = $data['description'];
   $path = $data['path'];
   $status = $data['status'];

   $result = $db->updateDesign($design_id, $name, $shortdescription, $description, $path, $status);
   if ($result) {
     //Design updated succesfully
     $response["error"] = false;
     $response["message"] = "Design updated succesfully";
   } else {
     //Design failed to update
     $response["error"] = true;
     $response["message"] = "Design failed to update. Please try again";
   }
   echoRespnse(200, $response);
 });

 $app->delete('/designs/:id', 'authenticate', function($design_id) use ($app) {
   $db = new DbHandler();
   $response = array();
   $result = $db->deleteDesigns($design_id);
   if ($result) {
     // Design deleted succesfully
     $response["error"] = false;
     $response["message"] = "Design deleted successfully";
   } else {
     //Design failed to delete
     $response["error"] = true;
     $response[" message"] = "Design failed to delete. Please try again";
   }
   echoRespnse(200, $response);
 })

 $app->get('/account', 'authenticate', function() use($app){
     global $user;

     $response = array();
     $db = new DbHandler();
     // fetching super agents by id
     $result = $db->getUsersById($user['id']);

     $response["error"] = false;
     $response["data"] = array();

     // looping through result and preparing super agents array
     while ($data = $result->fetch_assoc()) {
         $tmp = array();
         $tmp["name"] = $data["name"];
         $tmp["id"] = $data["id"];
         array_push($response["data"], $tmp);
     }

     echoRespnse(200, $response);
 });

 $app->get('/me', 'authenticate', function() use($app, $config, $db){
     $response = $app->response();
    $response->headers->set('Content-Type', 'application/json');
    $user_id = findUserId($app->request->headers->get($config->getAuthHeader()), $config->getSecret('TOKEN_SECRET'));
    $response->setBody(json_encode($bd->getUserWithId($user_id)));
 });

 $app->put('/me', function() use($app, $config, $db){
     $response = $app->response();
     $response->headers->set('Content-Type', 'application/json');
     $data = json_decode($app->request()->getBody(), true);
     $errors = array();

     if(count($errors) > 0){
         $json_errors = json_encode($errors);
         $response->setBody($json_errors);
         $response->setStatus(422);
         $app->stop();
     }
     $email = $data['email'];
     $user_id = findUserId($app->request->headers->get($config->getAuthHeader()), $config->getSecret('TOKEN_SECRET'));

     if(!empty($email)){
         if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
             array_push($errors, 'Invalid email');
         }
         if($db->userWithEmailExists($email, $user_id)){
             array_push($errors, 'Email already exists');
         }
     }

     if(count($errors) > 0){
         $json_errors = json_encode($errors);
         $response->setBody($json_errors);
         $response->setStatus(422);
         $app->stop();
     }
     $response->setBody('{"message": "User info updated"}');
 });
 $app->get('/authprovider', function() use($app, $db, $config){
     $user_id = findUserId($app->request->headers->get($config->getAuthHeader()), $config->getSecret('TOKEN_SECRET'));
     $response = $app->response();
     $response->setBody($db->getAuthProvider($user_id));
 });

 function findUserId($token, $secret){
     if($token){
         try {
             $payloadObject = JWT::decode($token, $secret);
             $payload = json_decode(json_encode($payloadObject), true);
             return $payload['sub'];
         }
         catch (Exception $e){
             return null;
         }
     }
     else {
         return null;
     }
 }

function create_token($issuer, $cid, $domain, $secret){
    $payload = array(
        'iss' => $issuer,
        'user' => $issuer,
        'cid' => $cid,
        'sub' => $domain,
        'iat' => time(),
        'exp' => time() + (2 * 7 * 24 * 60 * 60)
    );
    return JWT::encode($payload, $secret);
}

 function verify_password_strength($password){
     return (strlen($password) >= 8);
 }
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
