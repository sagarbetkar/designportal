<?php

function is_logged_in($token, $secret, $db){
    if($token){
        try {
            $payloadObject = JWT::decode($token, $secret);
            $payload = json_decode(json_encode($payloadObject), true);
            return $db->getUserWithId($payload['user']);
            if(!$db->getUserWithId($payload['user'])){
                return false;
            }
            return $payload;
        }
        catch (Exception $e){
            return false;
        }
    }
    else {
        return false;
    }

}

$app->hook('slim.before.router', function() use ($app, $config, $db){

    $token = $app->request->headers->get($config->getAuthHeader());
    $response = $app->response();
    $response->headers->set('Content-Type', 'application/json');

    $auth_needed = false;

    if (strpos($app->request()->getPathInfo(), '/me') === 0) {
        $auth_needed = true;
    }

    if (strpos($app->request()->getPathInfo(), '/authprovider') === 0) {
        $auth_needed = true;
    }

    if($auth_needed){
        if(!is_logged_in($token, $config->getSecret('TOKEN_SECRET'), $db)){
            $response->setStatus(401);
            $response->setBody('{"message": "Invalid token"}');
            $app->stop();
        }
    };
});

?>
