<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(name, email, password_hash, api_key, status) values(?, ?, ?, ?, 1)");
            $stmt->bind_param("ssss", $name, $email, $password_hash, $api_key);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT id, name, email, api_key, status, created_at FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id, $name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user by id
     * @param String $id user id
     */
    public function getUserWithId($id) {
         //Fetch Query
        $stmt = $this->conn->prepare("SELECT * FROM users where id=?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $tmp = array();

        while ($data = $result->fetch_assoc()) {
            $tmp["id"] = $data["id"];
            $tmp["name"] = $data["name"];
            $tmp["email"] = $data["email"];
            $tmp["status"] = $data["status"];
        }
        $stmt->close();

        return $tmp;
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Creating new agent
     * @param String $name Design name
     * @param String $shortdescription Design shortdescription
     * @param String $description Design description
     * @param String $path Design path
     * @param String $status Design status
     */

     public function createDesigns($name, $shortdescription, $description, $path, $status) {
       $response = array();

       $stmt = $this->conn->prepare("INSERT INTO designs(name, shortdescription, description, path, status) values(?, ?, ?, ?, ?)");
       $stmt->bind_param("sssss", $name, $shortdescription, $description, $path, $status);
       $result = $stmt->execute();
       $stmt->close();

       //check for successful insertion
       if ($result) {
         return DESIGN_UPLOADED_SUCCESSFULLY;
       } else {
         return DESIGN_UPLOADED_FAILED;
       }

       return $response;
     }

     /**
     * Fetching Designs
     */
     public function getDesigns() {
       $stmt = $this->conn->prepare("SELECT * FROM designs where status='Active'");
       $stmt->execute();
       $data = $stmt->get_result();
       $stmt->close();
       return $data;
     }

     /**
     * Fetching Designs by Id
     */
     public function getDesignsById($design_id) {
       //Fetch Query
       $stmt = $this->conn->prepare("SELECT * FROM designs where id=?");
       $stmt->bind_param('i',$design_id);
       $stmt->execute();
       $data = $stmt->get_result();
       $stmt->close();
       return $data;
     }

     /**
      * Updating designs
      * @param String $name Design name
      * @param String $shortdescription Design shortdescription
      * @param String $description Design description
      * @param String $path Design path
      * @param String $status Design status
      */
      public function updateDesigns($name, $shortdescription, $description, $path, $status) {
        $stmt = $this->conn->prepare("UPDATE designs SET name = ?, shortdescription = ?, description = ?, path = ?, status = ?");
        $stmt->bind_param("sssss", $name, $shortdescription, $description, $path, $status);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
      }

      /**
       * Deleting a design
       * @param String $design_id id of the agent to delete
       */
      public function deleteDesigns($design_id) {
           //Fetch Query
          $stmt = $this->conn->prepare("UPDATE designs SET status='Inactive' WHERE id = ?");
          $stmt->bind_param('i',$design_id);
          $stmt->execute();
          $num_affected_rows = $stmt->affected_rows;
          $stmt->close();
          return $num_affected_rows > 0;
      }

      /**
       * Fetching user by id
       * @param String $id user id
       */
       public function getSuperagentsById($id) {
           //Fetch Query
           $stmt = $this->conn->prepare("SELECT * FROM users where id=?");
           $stmt->bind_param('i',$id);
           $stmt->execute();
           $data = $stmt->get_result();
           $stmt->close();
           return $data;
       }
}

?>
