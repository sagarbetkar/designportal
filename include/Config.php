<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'designportal');

class Config{
    private $secrets;
    private $auth_header;
    public function __construct(){

        $this->secrets = array(
            'TOKEN_SECRET' => '',
        );

        $this->auth_header = 'Authorization';
    }

    public function getAuthHeader(){
        return $this->auth_header;
    }

    public function getSecret($secret_name){
        return $this->secrets[$secret_name];
    }

}

define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);

define('DESIGN_UPLOADED_SUCCESSFULLY',0);
define('DESIGN_UPLOADED_FAILED',1);
?>
